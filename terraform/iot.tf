resource "aws_iot_certificate" "cert" {
  active = true
}

resource "aws_iot_policy" "pubsub" {
  name = "PubSubIoT"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "iot:Connect"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "iot:Receive"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "iot:Publish"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "iot:Subscribe"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
})
}

resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}

resource "aws_iot_thing" "temp_sensor" {
  name = "temp_sensor"

  attributes = {
    First = "examplevalue"
  }
}

resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.temp_sensor.name
}

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}
# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "rule" {
  name        = "DBRuleY"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_roleY.arn
    put_item {
      table_name = aws_dynamodb_table.temperature.name
    }
  }
}

resource "aws_iot_topic_rule" "timestreamruleY" {
  name        = "DBtimestreamRuleY"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"
  timestream{
    database_name = aws_timestreamwrite_database.iotY.database_name
    role_arn = aws_iam_role.iot_roleY.arn
    table_name = aws_timestreamwrite_table.exampleY.table_name
    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }

  }

}
# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iam_role_policy" "test_policyY" {
  name = "test_policyY"
  role = aws_iam_role.iot_roleY.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:PutItem",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
