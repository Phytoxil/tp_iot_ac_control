# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "iotY" {
  database_name = "iotY"
}
# aws_timestreamwrite_table linked to the database
resource "aws_timestreamwrite_table" "exampleY" {
  database_name = aws_timestreamwrite_database.iotY.database_name
  table_name    = "temperaturesensor"
}